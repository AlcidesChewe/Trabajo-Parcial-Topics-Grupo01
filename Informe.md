![UPC LOGO](https://www.canvia.com/wp-content/uploads/2023/08/crymibfuwaapssb.png)

# Tópicos en Ciencias de la Computación - TRABAJO PARCIAL

## Información del Curso
**Profesor:** Luis Martin, Canaval Sanchez  
**Sección:** CC82  
**Fecha:** MAYO 2024

## Integrantes
- Charapaqui Reluz, Rommel Alcides - U202021294
- Barrionuevo Gutierrez, Daniel Ulises - U201922128
- Castilla Ochoa, Carlos Alonso - U202116277

## Aplicación de Programación por Restricciones (CSP) en la Asignación de Tripulación Aérea

### Introducción
En la presente tarea académica, nos proponemos abordar el desafiante problema de la asignación de tripulaciones aéreas a los vuelos de una aerolínea. Este problema implica no solo la óptima distribución del personal sino que también debe atender a diversas restricciones y preferencias, lo que lo convierte en un candidato ideal para la aplicación de técnicas de Programación por Restricciones (CSP).

### Problema y Motivación
El problema seleccionado para esta investigación surge de la necesidad de optimizar la asignación de los 20 miembros de la tripulación aérea a 10 vuelos diferentes, garantizando la cobertura adecuada de azafatas y auxiliares de vuelo por vuelo y asegurando que se cumplan los requerimientos lingüísticos y operativos. La motivación detrás de este estudio radica en mejorar la eficiencia operativa y la satisfacción del personal de la aerolínea, elementos críticos en la industria de la aviación.

![Tripulación aerea](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Austrian_Airlines_flight_attendant_and_passenger.jpg/1280px-Austrian_Airlines_flight_attendant_and_passenger.jpg)

### Objetivos
**Objetivo General:**  
Desarrollar un modelo de CSP que optimice la asignación de la tripulación aérea, satisfaciendo todas las restricciones y preferencias establecidas.

**Objetivos Específicos:**
- Identificar y definir las variables, dominios y restricciones relevantes para el modelo de CSP.
- Aplicar técnicas de programación por restricciones para la resolución del problema de asignación.
- Evaluar la eficacia del modelo propuesto mediante simulaciones y comparaciones con asignaciones actuales.

**Función Objeto:**  
Optimizar la asignación de los 20 miembros de la tripulación aérea a 10 vuelos diferentes, garantizando la cobertura adecuada y cumpliendo con los requerimientos lingüísticos y operativos, haciendo uso de CSP.

### Marco Teórico
1. **Fundamentos de la Programación por Restricciones (CSP):**
   - Definición de CSP: Presenta una explicación detallada sobre qué es CSP, incluyendo su historia y desarrollo en el campo de la inteligencia artificial.
   - Componentes clave de CSP: Variables, dominios y restricciones; cómo cada componente se utiliza para formular problemas y encontrar soluciones.

2. **Técnicas de Solución en CSP:**
   - Búsqueda y Retroceso (Backtracking): Describe el algoritmo de búsqueda y retroceso, una técnica fundamental en CSP que permite encontrar soluciones al probar combinaciones de valores y retrocediendo cuando se encuentra con una restricción que no se puede satisfacer.
   - Consistencia y Propagación de Restricciones: Explica cómo la propagación de restricciones se utiliza para simplificar el espacio de búsqueda y aumentar la eficiencia del proceso de solución.

3. **Aplicaciones de CSP en la Asignación y Planificación:**
   - Estudios de Caso Relacionados: Proporciona ejemplos de cómo CSP se ha utilizado exitosamente para resolver problemas similares en la asignación de recursos o planificación de horarios.

4. **Aspectos Computacionales de CSP:**
   - Solucionadores de CSP y Herramientas de Programación: Presenta las herramientas de software más comunes para trabajar con CSP, como MiniZinc o Choco Solver, y cómo se integran en ambientes de desarrollo como Python o Java.

5. **Relevancia del CSP en la Industria de la Aviación:**
   - Eficiencia Operativa y Satisfacción del Personal: Relaciona los beneficios potenciales de un sistema optimizado de asignación de tripulación con el rendimiento operativo y la moral del personal.

![Tripulación aerea](https://www.cs.us.es/~fsancho/Cursos/SVRAI/img/constraint-satisfaction-problems-n.jpg)

### Planteamiento del Problema
Para el planteamiento de este problema, consideraremos las siguientes variables y restricciones:
- **Variables:** Cada variable representará a un miembro de la tripulación y su asignación a un vuelo específico.
- **Dominios:** Los dominios estarán compuestos por los vuelos disponibles que cada miembro de la tripulación podría operar.
- **Restricciones:**
  - Restricciones de cantidad de personal por vuelo.
  - Requerimientos lingüísticos por vuelo.
  - Restricciones de no asignación múltiple para un mismo miembro de la tripulación.

Se detallará el modelo de CSP propuesto, incluyendo las funciones de filtrado y propagación de restricciones que reducirán el espacio de búsqueda y facilitarán la solución del problema.

### Desarrollo
El objetivo de este desarrollo es asignar eficientemente la tripulación aérea a varios vuelos de una aerolínea, cumpliendo con varias restricciones de personal, habilidades lingüísticas y asignaciones consecutivas. El modelo ha sido implementado utilizando `ortools.sat.python.cp_model`, un módulo de programación de restricciones de Google OR-Tools.

## Configuración del Modelo
Inicialmente, se configura el modelo y se definen las entidades principales:

- **Aircrew**: Diferenciado en stewards y air hostesses con listas de nombres.
- **Flights**: Una lista de vuelos numerados del 1 al 10.
- **Aircrew Numbers**: Número específico de tripulantes requeridos por vuelo.
- **Minimum Hostesses and Stewards**: Número mínimo de hostesses y stewards requeridos en cada vuelo.
- **Language Knowledge**: Diccionario que asocia idiomas específicos con miembros de la tripulación que los hablan.

```python
from ortools.sat.python import cp_model

def aircrew_assignment():
    model = cp_model.CpModel()
    ...
```
## Definición de Variables

Las variables del modelo son booleanas (`NewBoolVar`), donde cada variable representa si un miembro de la tripulación específico está asignado a un vuelo particular.

```python
from ortools.sat.python import cp_model

def aircrew_assignment():
    model = cp_model.CpModel()
    aircrew = {
        'stewards': ['Tom', 'David', 'Jeremy', 'Ron', 'Joe', 'Bill', 'Fred', 'Bob', 'Mario', 'Ed'],
        'air_hostesses': ['Carol', 'Janet', 'Tracy', 'Marilyn', 'Carolyn', 'Cathy', 'Inez', 'Jean', 'Heather', 'Juliet']
    }
    flights = list(range(1, 11))
    assignments = {}
    for flight in flights:
        for aircrew_type in aircrew:
            for member in aircrew[aircrew_type]:
                assignments[(flight, member)] = model.NewBoolVar(f'flight_{flight}_{member}')
```
## Restricciones del Modelo

El modelo incluye varias restricciones:

- **Número Total de Aircrew por Vuelo:**
Se asegura que el número total de tripulantes asignados a cada vuelo coincida con el número requerido.

- **Mínimo de Hostesses y Stewards:**
Cada vuelo debe tener al menos el número mínimo requerido de hostesses y stewards.

- **Habilidades Lingüísticas:**
Cada vuelo debe incluir al menos un miembro que hable los idiomas necesarios.

- **Asignaciones Consecutivas:**
Se prohíbe que los miembros de la tripulación estén en vuelos consecutivos, para evitar la fatiga y cumplir con las regulaciones laborales.

```python
for flight in flights:
    model.Add(sum(assignments[(flight, member)] for member in aircrew['stewards'] + aircrew['air_hostesses']) == aircrew_numbers[flight-1])
    ...
```
## Solución del Modelo

El modelo se resuelve utilizando el solver CpSolver de OR-Tools, y los resultados se imprimen mostrando qué miembros están en cada vuelo.

```python
solver = cp_model.CpSolver()
status = solver.Solve(model)
if status == cp_model.OPTIMAL:
    for flight in flights:
        print(f"Flight {flight}:")
        for member in aircrew['stewards'] + aircrew['air_hostesses']:
            if solver.Value(assignments[(flight, member)]) == 1:
                print(f"\t{member}")
else:
    print("No solution found.")
```
## Conclusiones

El uso de OR-Tools y la programación de restricciones han permitido desarrollar un modelo robusto y flexible para la asignación de tripulaciones aéreas. Este enfoque garantiza el cumplimiento de todas las regulaciones operativas y preferencias de lenguaje, maximizando la eficiencia operativa y la satisfacción de la tripulación.

## Referencias


[1] Lindstrom, G. (1985, January). Functional programing and the logical variable. In Proceedings of the 12th ACM SIGACT-SIGPLAN symposium on Principles of programming languages (pp. 266-280).


[2] Rossi, F., van Beek, P., & Walsh, T. (Eds.). (2006). Handbook of Constraint Programming. Elsevier. Rossi, F., van Beek, P., & Walsh, T. (Eds.). (2006). Handbook of Constraint Programming. Elsevier. Rardin, R. L. (1998). Optimization in Operations Research. Prentice Hall.


[3] Rardin, R. L. (1998). Optimization in Operations Research. Prentice Hall. Mercier, A., & Soumis, F. (2001). Airline crew scheduling: From planning to operation. Journal of Scheduling, 4(3), 263-279. Mercier, A., & Soumis, F. (2001). Airline crew scheduling: From planning to operation. Journal of Scheduling, 4(3), 263-279.
